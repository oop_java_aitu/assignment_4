package com.company;

import com.company.controllers.MedicineController;

import java.sql.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MyApplication {
    private final MedicineController controller;
    private final Scanner scanner;

    public MyApplication(MedicineController controller) {
        this.controller = controller;
        scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");
    }

    public void start() {
        while (true) {
            System.out.println();
            System.out.println("Welcome to My Application");
            System.out.println("Select option:");
            System.out.println("1. Search for any medicine by name");
            System.out.println("2. Get medicine by id");
            System.out.println("3. Create medicine");
            System.out.println("4. Delete medicine by id");
            System.out.println("5. Get all medicine");
            System.out.println("0. Exit");
            System.out.println();
            try {
                System.out.print("Enter option (1-3): ");
                int option = scanner.nextInt();
                if (option == 1) {
                    getSearchByNameMenu();
                } else if (option == 2) {
                    getMedicineByIdMenu();
                } else if (option == 3) {
                    createMedicineMenu();
                } else if (option == 4) {
                    deleteByIdMenu();
                } else if (option == 5) {
                    getAllMedicinesMenu();
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Input must be integer");
                scanner.nextLine(); // to ignore incorrect input
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }

            System.out.println("*************************");

        }
    }

    public void deleteByIdMenu() {
        System.out.println("Please enter id");
        int id = scanner.nextInt();
        String response = controller.deleteMedicine(id);
        System.out.println(response);
    }

    public void getSearchByNameMenu() {
        System.out.println("Please enter name");
        String name = scanner.next();

        String response = controller.searchByNameGetAllMedicines(name);
        System.out.println(response);
    }

    public void getAllMedicinesMenu() {
        String response = controller.getAllMedicines();
        System.out.println(response);
    }

    public void getMedicineByIdMenu() {
        System.out.println("Please enter id");

        int id = scanner.nextInt();
        String response = controller.getMedicine(id);
        System.out.println(response);
    }

    public void createMedicineMenu() {
        System.out.println("Please enter name");
        String name = scanner.next();
        System.out.println("Please enter price");
        String price = scanner.next();
        System.out.println("Please enter expirationDate");
        String expirationDate = scanner.next();
        System.out.println("Please enter manufacturer");
        String manufacturer = scanner.next();
        System.out.println("Please enter extraInfo");
        String extraInfo = scanner.next();

        String response = controller.createMedicine(name, Integer.parseInt(price), Date.valueOf(expirationDate), manufacturer, extraInfo);
        System.out.println(response);
    }
}
