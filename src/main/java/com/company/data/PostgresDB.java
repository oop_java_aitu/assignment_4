package com.company.data;

import com.company.data.interfaces.IDB;

import java.sql.*;

public class PostgresDB implements IDB {
//    @Override
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        String connectionUrl = "jdbc:postgresql://localhost:5432/postgres";
        try {
            // Here we load the driver’s class file into memory at the runtime
            Class.forName("org.postgresql.Driver");

            // Establish the connection
//            return DriverManager.getConnection(connectionUrl, "postgres", "0000");
            return DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
