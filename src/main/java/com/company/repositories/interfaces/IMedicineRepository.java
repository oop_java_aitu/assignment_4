package com.company.repositories.interfaces;

import com.company.entities.Medicine;

import java.util.List;

public interface IMedicineRepository {
    boolean createMedicine(Medicine medicine);
    boolean deleteMedicine(int id);
    Medicine getMedicine(int id);
    List<Medicine> getAllMedicines();
    List<Medicine> searchByNameGetAllMedicines(String name);
}
