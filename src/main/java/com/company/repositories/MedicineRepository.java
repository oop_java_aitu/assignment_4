package com.company.repositories;

import com.company.data.interfaces.IDB;
import com.company.entities.Medicine;
import com.company.repositories.interfaces.IMedicineRepository;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class MedicineRepository implements IMedicineRepository {
    private final IDB db;

    public MedicineRepository(IDB db) {
        this.db = db;
    }

    @Override
    public boolean createMedicine(Medicine medicine) {
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "INSERT INTO medicines(name,price,expiration_date,manufacturer,extra_info) VALUES (?,?,?,?,?)";
            PreparedStatement st = con.prepareStatement(sql);

            st.setString(1, medicine.getName());
            st.setInt(2, medicine.getPrice());
            st.setDate(3, medicine.getExpirationDate());
            st.setString(4, medicine.getManufacturer());
            st.setString(5, medicine.getExtraInfo());

            st.execute();
            return true;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean deleteMedicine(int id) {
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "DELETE FROM medicines WHERE id=?";
            PreparedStatement st = con.prepareStatement(sql);

            st.setInt(1, id);
            st.executeUpdate();

            return true;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            return false;
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    public Medicine getMedicine(int id) {
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "SELECT id,name,price,expiration_date,manufacturer,extra_info FROM medicines WHERE id=?";
            PreparedStatement st = con.prepareStatement(sql);

            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Medicine(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getDate("expiration_date"),
                        rs.getString("manufacturer"),
                        rs.getString("extra_info"));
            }
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public List<Medicine> getAllMedicines() {
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "SELECT id,name,price,expiration_date,manufacturer,extra_info FROM medicines";
            Statement st = con.createStatement();

            ResultSet rs = st.executeQuery(sql);
            List<Medicine> medicines = new LinkedList<>();
            while (rs.next()) {
                Medicine medicine = new Medicine(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getDate("expiration_date"),
                        rs.getString("manufacturer"),
                        rs.getString("extra_info"));
                medicines.add(medicine);
            }

            return medicines;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public List<Medicine> searchByNameGetAllMedicines(String name) {
        Connection con = null;

        try {
            con = db.getConnection();
            String sql = "SELECT id,name,price,expiration_date,manufacturer,extra_info FROM medicines WHERE name ilike ?;";

            PreparedStatement st = con.prepareStatement(sql);

            st.setString(1, '%' + name + '%');

            ResultSet rs = st.executeQuery();

            List<Medicine> medicines = new LinkedList<>();
            while (rs.next()) {
                Medicine medicine = new Medicine(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getDate("expiration_date"),
                        rs.getString("manufacturer"),
                        rs.getString("extra_info"));
                medicines.add(medicine);
            }

            return medicines;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                assert con != null;
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    }
}
