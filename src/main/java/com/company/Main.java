package com.company;

import com.company.controllers.MedicineController;
import com.company.data.PostgresDB;
import com.company.data.interfaces.IDB;
import com.company.repositories.MedicineRepository;
import com.company.repositories.interfaces.IMedicineRepository;

public class Main {
    public static void main(String[] args) {
        IDB db = new PostgresDB();
        IMedicineRepository repo = new MedicineRepository(db);
        MedicineController controller = new MedicineController(repo);
        MyApplication app = new MyApplication(controller);
        app.start();
    }
}
