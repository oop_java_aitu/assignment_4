package com.company.controllers;

import com.company.entities.Medicine;
import com.company.repositories.interfaces.IMedicineRepository;

import java.sql.Date;
import java.util.List;
import java.util.Objects;


public class MedicineController {
    private final IMedicineRepository repo;

    public MedicineController(IMedicineRepository repo) {
        this.repo = repo;
    }

    public String deleteMedicine(int id) {
        if (Objects.equals(getMedicine(id), "Medicine was not found!")) {
            return "Medicine not found to delete";
        }

        boolean deleted = repo.deleteMedicine(id);

        return (deleted ? "Medicine was deleted!" : "Medicine deletion was failed!");
    }

    public String searchByNameGetAllMedicines(String name) {
        List<Medicine> medicines = repo.searchByNameGetAllMedicines(name);

        return medicines.toString();
    }

    public String createMedicine(String name, int price, Date expirationDate, String manufacturer, String extraInfo) {
        Medicine medicine = new Medicine(name, price, expirationDate, manufacturer, extraInfo);

        boolean created = repo.createMedicine(medicine);

        return (created ? "Medicine was created!" : "Medicine creation was failed!");
    }

    public String getMedicine(int id) {
        Medicine medicine = repo.getMedicine(id);

        return (medicine == null ? "Medicine was not found!" : medicine.toString());
    }

    public String getAllMedicines() {
        List<Medicine> medicines = repo.getAllMedicines();

        return medicines.toString();
    }
}
