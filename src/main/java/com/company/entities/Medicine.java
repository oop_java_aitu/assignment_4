package com.company.entities;

import java.sql.Date;

public class Medicine {
    private int id;
    private String name;
    private int price;
    private Date expirationDate;
    private String manufacturer;
    private String extraInfo;

    public Medicine() {

    }

    public Medicine(String name, int price, Date expirationDate, String manufacturer, String extraInfo) {
        setName(name);
        setPrice(price);
        setExpirationDate(expirationDate);
        setManufacturer(manufacturer);
        setExtraInfo(extraInfo);
    }

    public Medicine(int id, String name, int price, Date expirationDate, String manufacturer, String extraInfo) {
        setId(id);
        setName(name);
        setPrice(price);
        setExpirationDate(expirationDate);
        setManufacturer(manufacturer);
        setExtraInfo(extraInfo);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", expirationDate=" + expirationDate +
                ", manufacturer='" + manufacturer + '\'' +
                ", extraInfo='" + extraInfo + '\'' +
                '}';
    }
}
